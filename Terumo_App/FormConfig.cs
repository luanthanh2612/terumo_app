﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.IO;

namespace Terumo_App
{
    public partial class FormConfig : Form
    {
        public FormConfig()
        {
            InitializeComponent();
            
        }

        private void btnupdate_Click(object sender, EventArgs e)
        {
            if (txtIP.Text.Trim().Equals(""))
            {
                MessageBox.Show("Ip must be input", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
            }
            else
            {
                if (AppConfig.insertIpToFile(txtIP.Text))
                {
                    MessageBox.Show("Update successful", "Infomation", MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
                }
                else
                {
                    MessageBox.Show("Error occurred", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                }
            }



        }

        private void btnPrevi_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void FormConfig_Load(object sender, EventArgs e)
        {
            txtIP.Text = AppConfig.createFolderWithFile();
        }
    }
}