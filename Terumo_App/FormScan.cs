﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Terumo_App
{
    public partial class FormScan : Form
    {
        public FormScan()
        {
            InitializeComponent();

        }

        private void txtQtyIn_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormScan_Load(object sender, EventArgs e)
        {
            FileUtil fileUtil = new FileUtil();
            string configPath =  AppConfig.getIpConfig();

            string data = fileUtil.loadFileFromHost(configPath);
            MessageBox.Show(data);

        }

     

    }
}