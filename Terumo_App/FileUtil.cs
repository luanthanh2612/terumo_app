﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Terumo_App
{
    class FileUtil
    {

        public string loadFileFromHost(string ip)
        {
            try
            {
                string line;
                FileStream fs = new FileStream(ip, FileMode.OpenOrCreate);

                StreamReader sr = new StreamReader(fs);
                line = sr.ReadLine();

                sr.Close();
                return line;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return e.Message;
            }


        }


    }
}
