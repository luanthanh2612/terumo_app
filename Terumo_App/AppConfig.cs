﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;

namespace Terumo_App
{
    class AppConfig
    {

        public static string createFolderWithFile()
        {
            try
            {
                string ip = "";
                string assemblyLocation = Assembly.GetExecutingAssembly().GetName().CodeBase;
                string currentDirectory = Path.Combine(@"\\Program Files", "\\ConfigFile.txt");

                if (!File.Exists(currentDirectory))
                {
                    File.Create(currentDirectory);
                }
                else
                {
                    StreamReader sr = new StreamReader(currentDirectory);
                    ip = sr.ReadLine();

                    sr.Close();
                }

                return ip;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);

                return e.Message;
            }
        }

        public static string getIpConfig()
        {
            string ip = "";
            string currentDirectory = Path.Combine(@"\\Program Files", "\\ConfigFile.txt");
            try
            {
                StreamReader sr = new StreamReader(currentDirectory);
                ip = sr.ReadLine();

                sr.Close();

                return ip;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static bool insertIpToFile(string ip)
        {
            string currentDirectory = Path.Combine(@"\\Program Files", "\\ConfigFile.txt");

            try
            {
                StreamWriter sw = new StreamWriter(currentDirectory);
                sw.Write(ip);

                sw.Flush();
                sw.Close();


                return true;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return false;
            }
        }
    }
}
